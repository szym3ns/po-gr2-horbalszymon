package pl.edu.uwm.wmii.horbalszymon.laboratorium02;
import java.util.Scanner;
import java.util.Random;

public class Z1 {
    private static Scanner read = new Scanner(System.in);
    private static Random rand = new Random();
    private static int tab[];
    private static int n = 0, ileP = 0, ileF = 0, ileDodatnich = 0, ileUjemnych = 0, ileZer = 0, najwieksza = 0, ileNajwieksza = 0,
                        sumaUjemnych = 0, sumaDodatnich = 0, najdluzszyOdcinekDodatnich = 0;

    public static void main(String[] args) {
        while (!(1 <= n && n <= 100))
            n = read.nextInt();

        tab = new int[n];
        for (int i = 0; i < tab.length; i++)
            tab[i] = getRandomNumberInRange(-999, 999);
        draw();
        podpunktA();
        podpunktB();
        podpunktC();
        podpunktD();
        podpunktE();
        System.out.printf("\nIlosc liczb parzystych: %d\nIlosc liczb nieparzystych: %d\n", ileP, ileF);
        System.out.printf("Ilosc liczb dodatnich: %d\nIlosc liczb ujemnych: %d\nIlosc zer: %d\n", ileDodatnich, ileUjemnych, ileZer);
        System.out.printf("Element najwiekszy: %d   Wystapil %d razy\n", najwieksza, ileNajwieksza);
        System.out.printf("Suma dodatnich elementow: %d\nSuma ujemnych elementow: %d\n", sumaDodatnich, sumaUjemnych);
        System.out.printf("Najdluzszy fragment dodatnich elementow: %d\n", najdluzszyOdcinekDodatnich);
        podpunktF();
        podpunktG();


    }
    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max)
            throw new IllegalArgumentException("Max must be greater than min");
        return rand.nextInt((max - min) + 1) + min;
    }
    private static void draw()
    {
        for (int x : tab)
            System.out.printf("%d ", x);
    }

    // PODPUNKTY
    private static void podpunktA()
    {
        for (int x : tab)
        {
            if (x % 2 == 0)
                ++ileP;
            else
                ++ileF;
        }
    }
    private static void podpunktB()
    {
        for (int x : tab)
        {
            if (x > 0)
                ++ileDodatnich;
            else if (x == 0)
                ++ileZer;
            else
                ++ileUjemnych;
        }
    }
    private static void podpunktC()
    {
        int tmp = tab[0];
        for (int x : tab)
            if (x > tmp)
                tmp = x;

        najwieksza = tmp;

        for (int x : tab)
            if (x == tmp)
                ++najwieksza;

    }
    private static void podpunktD()
    {
        for (int x : tab)
        {
            if (x > 0)
                sumaDodatnich += x;
            else if (x < 0)
                sumaUjemnych += x;
        }
    }
    private static void podpunktE()
    {
        int tmp = 0;
        for (int x : tab)
        {
            if (x >= 0)
                ++tmp;
            else
                tmp = 0;

            if (tmp > najdluzszyOdcinekDodatnich)
                najdluzszyOdcinekDodatnich = tmp;
        }
    }
    private static void podpunktF()
    {
        for (int i = 0; i < tab.length; i++)
        {
            if (tab[i] > 0)
                tab[i] = 1;
            else if (tab[i] < 0)
                tab[i] = -1;
        }
        System.out.print("Tablica: ");
        for (int x : tab)
            System.out.printf("%d ", x);
    }
    private static void podpunktG()
    {
        System.out.println("\nWczytaj liczbe do zmiennej lewy: ");
        int lewy = read.nextInt();
        System.out.println("Wczytaj liczbe do zmiennej prawy: ");
        int prawy = read.nextInt();

        for (int i = lewy - 1 ; i < prawy / 2 ; i++)
        {
            int temp = tab[i];
            tab[i] = tab[prawy - i - 1];
            tab[prawy - i - 1] = temp;
        }
        draw();
    }
}

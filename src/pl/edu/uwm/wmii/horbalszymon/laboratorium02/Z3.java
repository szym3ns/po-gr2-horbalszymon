package pl.edu.uwm.wmii.horbalszymon.laboratorium02;
import java.util.Scanner;
import java.util.Random;

public class Z3 {
    private static Scanner read = new Scanner(System.in);
    private static Random rand = new Random();
    private static int m = 0, n = 0, k = 0;

    public static void main(String[] args)
    {
           while (!(m >= 1 && m <= 10))
           {
               System.out.print("Podaj m[1;10]: ");
               m = read.nextInt();
           }
            while (!(n >= 1 && n <= 10))
            {
                System.out.print("Podaj n[1;10]: ");
                n = read.nextInt();
            }
            while (!(k >= 1 && k <= 10))
            {
                System.out.print("Podaj k[1;10]: ");
                k = read.nextInt();
            }

            int m1[][] = new int[m][n];
            int m2[][] = new int[n][k];
            int m3[][] = new int[m][k];

            for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                    m1[i][j] = rand.nextInt(10) + 1;

            for (int i = 0; i < n; i++)
                for (int j = 0; j < k; j++)
                    m2[i][j] = rand.nextInt(10) + 1;
            draw(m1);
            draw(m2);

            for(int i = 0 ; i < m; i++)
                for(int j=0; j < k ; j++)
                {
                    int tmp = 0;
                    for(int t = 0; t < n; t++)
                        tmp += m1[i][t] * m2[t][j];
                    m3[i][j] = tmp;
                }
                draw(m3);
    }
    private static void draw(int tab[][])
    {
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[0].length; j++)
                System.out.printf("%d ", tab[i][j]);
            System.out.print("\n");
        }
        System.out.print("\n");
    }
}

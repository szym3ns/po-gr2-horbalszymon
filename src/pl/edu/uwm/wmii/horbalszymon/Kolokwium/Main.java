package pl.edu.uwm.wmii.horbalszymon.Kolokwium;

import java.time.LocalDate;
import java.util.ArrayList;

public class Main
{
    private static ArrayList<Komputer> groupComputers = new ArrayList<Komputer>();
    private static ArrayList<Laptop> groupLaptops = new ArrayList<Laptop>();

    private static LocalDate data1 = LocalDate.of(2017, 05, 13);
    private static LocalDate data2 = LocalDate.of(2015, 12, 22);
    private static String name1 = "Apple";
    private static String name2 = "Asus";

    public static void main (String[] args)
    {
        addToList();
        printLists();
    }

    private static void addToList()
    {
        groupComputers.add(new Komputer(name1, data1));
        groupComputers.add(new Komputer(name1, data2));
        groupComputers.add(new Komputer(name2, data1));
        groupComputers.add(new Komputer(name2, data2));
        groupComputers.add(new Komputer(name1, data2));

        groupLaptops.add(new Laptop (name1, data1, true));
        groupLaptops.add(new Laptop (name1, data2, false));
        groupLaptops.add(new Laptop (name2, data1, false));
        groupLaptops.add(new Laptop (name2, data2, false));
        groupLaptops.add(new Laptop (name1, data2, true));
    }

    private static void printLists()
    {
        for (Komputer item : groupComputers)
            System.out.println(item.getNazwa() + "\t" + item.getDataProdukcji());

        groupComputers.sort(Komputer::compareTo);
        System.out.println();

        for (Komputer item : groupComputers)
            System.out.println(item.getNazwa() + "\t" + item.getDataProdukcji());

        System.out.println();
        for (Laptop item : groupLaptops)
            System.out.println(item.getNazwa() + "\t" + item.getDataProdukcji() + "\t APPLE: " + item.getCzyApple());

        groupLaptops.sort(Laptop::compareTo);
        System.out.println();

        for (Laptop item : groupLaptops)
            System.out.println(item.getNazwa() + "\t" + item.getDataProdukcji() + "\t APPLE: " + item.getCzyApple());
    }
}

package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z1a {

    private static Scanner read = new Scanner(System.in);

    public static void main(String[] args)
    {
        System.out.print("Podaj n: ");
        int n = read.nextInt();
        int suma = 0;

        for (int i = 0; i < n; i++)
        {
            System.out.print("Podaj liczbe: ");
            suma += read.nextDouble();
        }
        System.out.printf("Suma liczb wynosi: %d", suma);

    }
}
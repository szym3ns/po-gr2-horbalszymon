package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z1f {
    private static Scanner read = new Scanner(System.in);
    private static int a;

    public static void main(String[] args) {
        a = read.nextInt();
        int wynik = 0;

        for(int i = 0; i <= a; i++)
        {
            wynik += Math.pow(i, 2);
        }
        System.out.printf("Wynik wynosi: %d ", wynik);
    }
}
package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z21e {
    private static Scanner read = new Scanner(System.in);
    private static  int ilosc = 0, x = 0, silnia = 1;

    public static void main(String[] args)
    {
        System.out.print("Podaj n: ");
        int n = read.nextInt();

        for(int i = 0; i < n; i++)
        {
            for (int j = 1; j <= i+1; j++)
                silnia *= j;
            System.out.print("Podaj liczbe: ");
            x = read.nextInt();
            if(Math.pow(2, i + 1) < x && x < silnia)
                ilosc++;
        }
        System.out.printf("\nSpelniajace warunek: %d", ilosc);
    }
}
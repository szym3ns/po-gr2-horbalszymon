package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z21c {
    private static Scanner read = new Scanner(System.in);
    private static int ilosc = 0;

    public static void main(String[] args)
    {
        System.out.print("Podaj n: ");
        int n = read.nextInt();

        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj liczbe: ");
            int x = read.nextInt();
            double a = Math.sqrt((double)x);

            if(a % 2 == 0)
                ++ilosc;
        }
        System.out.printf("Kwadraty liczb parzystych: %d", ilosc);

    }
}

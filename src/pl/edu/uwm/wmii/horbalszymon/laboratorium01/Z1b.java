package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z1b {
    private static Scanner read = new Scanner(System.in);
    private static int a, b;

    public static void main(String[] args)
    {
        a = read.nextInt();
        int result = 1;

        for(int i = 1; i < a + 1; i++)
        {
            b = read.nextInt();
            result *= b;
        }
        System.out.printf("Wynik wynosi: %d", result);
    }
}
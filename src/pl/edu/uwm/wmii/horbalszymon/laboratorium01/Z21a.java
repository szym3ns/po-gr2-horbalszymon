package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z21a {
    private static Scanner read = new Scanner(System.in);
    private static int a, wynik = 0;

    public static void main(String[] args)
    {
        a = read.nextInt();

        for(int i = 1; i <= a; i++)
            if(i % 2 != 0)
                wynik += 1;
        System.out.printf("Wynik to: %d", wynik);
    }
}
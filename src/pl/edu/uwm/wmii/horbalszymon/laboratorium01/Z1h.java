package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z1h {
    private static Scanner read = new Scanner(System.in);
    private static int a, wynik = 0;

    public static void main(String[] args)
    {
        a = read.nextInt();
        for(int i = 0; i <= a; i++)
        {
            if(i % 2 == 0)
                wynik += i;
            else
                wynik += -i;
        }
        System.out.printf("Wynik wynosi: %d", wynik);
    }
}
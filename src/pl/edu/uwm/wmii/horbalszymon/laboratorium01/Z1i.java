package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z1i {
    private static Scanner read = new Scanner(System.in);
    private static int a, silnia = 1;
    private static double wynik = 0;

    public static void main(String[] args)
    {
        a = read.nextInt();
        for(int i = 1; i <= a; i++)
            silnia *= i;

        for(int i = 1; i <= a; i++)
        {
            if (i % 2 == 0)
                wynik += (i / (silnia));
            else
                wynik += -(i / (silnia));
        }
        System.out.printf("Wynik wynosi: %f", wynik);
    }
}
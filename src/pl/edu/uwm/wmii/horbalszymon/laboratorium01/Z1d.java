package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z1d {
    private static int a;
    private static Scanner read =new Scanner(System.in);

    public static void main(String[] args)
    {
        a = read.nextInt();
        int wynik = 0;

        for(int i = 0; i < a + 1; i++)
            wynik += Math.sqrt(Math.abs(i));
        System.out.printf("Wynik wynosi: %d ", wynik);
    }
}

package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z1c {
        private static Scanner read = new Scanner(System.in);
        private static int a;

        public static void main(String[] args)
        {

            a = read.nextInt();
            int result = 0;
            for(int i = -10; i < a + 1; i++)
            {
                result += Math.abs(i);
            }
            System.out.printf("Wynik wynosi: %d", result);
        }
}

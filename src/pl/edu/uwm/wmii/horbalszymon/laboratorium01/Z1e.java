package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z1e {
    private static Scanner read = new Scanner(System.in);
    private static int a;

    public static void main(String[] args) {

        a = read.nextInt();
        int wynik = 1;
        for(int i = 1; i <= a; i++)
        {
            wynik *= Math.abs(i);
        }
        System.out.printf("Wynik wynosi: %d", wynik);
    }
}
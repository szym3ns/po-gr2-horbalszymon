package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z12 {
    private static Scanner read = new Scanner(System.in);
    private static int n, tab[];

    public static void main(String[] args)
    {
        n = read.nextInt();
        tab = new int[n];
        for (int i = 0; i < n; i++)
            tab[i] = read.nextInt();
        for (int i = 1; i < n; i++)
            System.out.printf("%d ", tab[i]);
        System.out.printf("%d ", tab[0]);

    }
}

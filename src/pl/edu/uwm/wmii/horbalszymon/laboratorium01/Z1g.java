package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z1g {
    private static Scanner read = new Scanner(System.in);
    private static int a;

    public static void main(String[] args)
    {
        a = read.nextInt();
        int tmp1 = 0;
        int tmp2 = 1;
        for(int i = 1; i <= a; i++)
        {
            tmp1 += i;
            tmp2 *= i;
        }
        System.out.printf(" Wynik dodawania: %d\n Wynik mnozenia: %d", tmp1, tmp2);
    }
}
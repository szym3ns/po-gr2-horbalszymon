package pl.edu.uwm.wmii.horbalszymon.laboratorium01;
import java.util.Scanner;

public class Z21b {
    private static Scanner read = new Scanner(System.in);
    private static int ilosc = 0;

    public static void main(String[] args)
    {
        int n = read.nextInt();

        for(int i = 0; i < n; i++)
        {
            System.out.print("Podaj liczbe: ");
            int x = read.nextInt();

            if(x % 3 == 0 && x % 5 != 0)
                ilosc++;
        }
        System.out.printf("Ilosc podzielnych przez 3, ale niepodzielnych przez 5: %d", ilosc);

    }
}
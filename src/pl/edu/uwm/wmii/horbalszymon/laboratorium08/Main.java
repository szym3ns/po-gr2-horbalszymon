package pl.edu.uwm.wmii.horbalszymon.laboratorium08;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args){
        Osoba os = new Osoba();
        os.setNazwisko("Kaczmarczyk");
        os.setDataUrodzenia(LocalDate.parse("2018-12-23"));
        System.out.println(os.toString());
    }
}
